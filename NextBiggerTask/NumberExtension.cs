﻿using System;

namespace NextBiggerTask
{
    public static class NumberExtension
    {
        #pragma warning disable
        public static int? NextBiggerThan(int number)
        {
            char[] numString = number.ToString().ToCharArray();

            if (number < 0 || number == int.MinValue)
            {
                throw new ArgumentException($"Value of {nameof(number)} cannot be less zero.");
            }
            else if (number == int.MaxValue)
            {
                return null;
            }

            for (int i = numString.Length - 1; i > 0; i--)
            {
                if (numString[i - 1] < numString[i])
                {
                    int temp1 = numString[i];
                    numString[i] = numString[i - 1];
                    numString[i - 1] = (char)temp1;

                    for (int k = i; k < numString.Length - 1; k++)
                    {
                        for (int j = k + 1; j < numString.Length; j++)
                        {
                            if (numString[k] > numString[j])
                            {
                                int temp2 = numString[k];
                                numString[k] = numString[j];
                                numString[j] = (char)temp2;
                            }
                        }
                    }

                    return int.Parse(numString);
                }
            }

            return null;
        }
    }
}
